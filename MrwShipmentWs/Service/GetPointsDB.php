<?php

namespace Interactiv4\MrwShipmentWs\Service;

use Exception;
use Interactiv4\MrwShipmentWs\Model\Config;
use Psr\Log\LoggerInterface;
use SoapClient;
use SoapFault;
use SoapHeader;

class GetPointsDB
{
    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param Config $config
     * @param LoggerInterface $logger
     */
    public function __construct(Config $config, LoggerInterface $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function execute(): array
    {
        $wsdlUrl = $this->config->getApiUrl();

        try {
            $client = $this->createSoapClient($wsdlUrl);

            $headerData = [
                'CodigoFranquicia' => $this->config->getFranchiseCode(),
                'CodigoAbonado' => $this->config->getSubscriberCode(),
                'CodigoDepartamento' => $this->config->getInternalDepartment(),
                'UserName' => $this->config->getUser(),
                'Password' => $this->config->getPassword()
            ];

            $header = new SoapHeader($wsdlUrl, 'AuthInfo', $headerData);

            $client->__setSoapHeaders($header);

            $response = $client->__soapCall('PointsDB', []);


            $resultArray = $this->convertResponseToArray($response->GetPointsDBResult);

            return $resultArray;
        } catch (Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }

        return [];
    }

    /**
     *
     */
    public function test()
    {
        $test = $this->execute();

        var_dump($test);
    }

    /**
     * @param string $wsdlUrl
     * @param false $trace
     * @return SoapClient
     * @throws SoapFault
     */
    private function createSoapClient(string $wsdlUrl, $trace = false): SoapClient
    {
        return new SoapClient($wsdlUrl, ['trace' => $trace]);
    }

    /**
     * @param $responseObject
     * @return array
     */
    private function convertResponseToArray($responseObject): array
    {
        $array = (array)$responseObject;

        foreach ($array as $key => &$field) {
            if (is_object($field)) {
                $field = $this->convertResponseToArray($field);
            }
        }
        return $array;
    }
}
