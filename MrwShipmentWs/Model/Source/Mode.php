<?php

namespace Interactiv4\MrwShipmentWs\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 *
 */
class Mode implements ArrayInterface
{
    public const MODE_TEST_CODE = 'test';
    public const MODE_TEST_LABEL = 'Test';
    public const MODE_PRODUCTION_CODE = 'production';
    public const MODE_PRODUCTION_LABEL = 'Production';

    public function toOptionArray()
    {
        return [
            ['value' => self::MODE_TEST_CODE, 'label' => __(self::MODE_TEST_LABEL)],
            ['value' => self::MODE_PRODUCTION_CODE, 'label' => __(self::MODE_PRODUCTION_LABEL)],
        ];
    }
}
