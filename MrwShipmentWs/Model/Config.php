<?php

namespace Interactiv4\MrwShipmentWs\Model;

use Interactiv4\MrwShipmentWs\Model\Source\Mode;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{
    public const XML_PATH_STATUS = 'MrwShipmentWS/settings/status';
    public const XML_PATH_MODE = 'MrwShipmentWS/settings/mode';
    public const XML_PATH_TEST_URL = 'MrwShipmentWS/settings/test_url';
    public const XML_PATH_PRODUCTION_URL = 'MrwShipmentWS/settings/production_url';
    public const XML_PATH_USER = 'MrwShipmentWS/settings/user';
    public const XML_PATH_PASSWORD = 'MrwShipmentWS/settings/password';
    public const XML_PATH_MONITORING_PASSWORD = 'MrwShipmentWS/settings/monitoring_password';
    public const XML_PATH_FRANCHISE_CODE = 'MrwShipmentWS/settings/franchise_code';
    public const XML_PATH_SUBSCRIBER_CODE = 'MrwShipmentWS/settings/subscriber_code';
    public const XML_PATH_SUBSCRIBER_NAME = 'MrwShipmentWS/settings/subscriber_name';
    public const XML_PATH_INTERNAL_DEPARTMENT = 'MrwShipmentWS/settings/internal_department';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param int|null $storeId
     * @return string
     */
    public function isEnabled(?int $storeId = null): string
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_STATUS);
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getMode(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_MODE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getTestUrl(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_TEST_URL,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getProductionUrl(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_PRODUCTION_URL,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getUser(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_USER,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getPassword(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_PASSWORD,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getMonitoringPassword(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_MONITORING_PASSWORD,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getFranchiseCode(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_FRANCHISE_CODE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getSubscriberCode(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_SUBSCRIBER_CODE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getSubscriberName(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_SUBSCRIBER_CODE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @param int|null $storeId
     * @return string|null
     */
    public function getInternalDepartment(?int $storeId = null): ?string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_INTERNAL_DEPARTMENT,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $value;
    }

    /**
     * @return string|null
     */
    public function getApiUrl(): ?string
    {
        if ($this->getMode() == Mode::MODE_PRODUCTION_CODE) {
            return $this->getProductionUrl();
        }

        return $this->getTestUrl();
    }

}
