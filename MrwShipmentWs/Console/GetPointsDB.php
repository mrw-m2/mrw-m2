<?php

namespace Interactiv4\MrwShipmentWs\Console;

use Exception;
use Interactiv4\MrwShipmentWs\Service\GetPointsDB as ServiceGetPointsDB;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 */
class GetPointsDB extends Command
{
    /**
     * @var ServiceGetPointsDB
     */
    private ServiceGetPointsDB $serviceGetPointsDB;

    /**
     * @param ServiceGetPointsDB $serviceGetPointsDB
     */
    public function __construct(ServiceGetPointsDB $serviceGetPointsDB)
    {
        $this->serviceGetPointsDB = $serviceGetPointsDB;
        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setName('mrw_shipment:GetPointsDB:test');
        $this->setDescription('Test MRW Shipment API. GetPointsDB Method');

        parent::configure();
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->serviceGetPointsDB->test();
    }
}
